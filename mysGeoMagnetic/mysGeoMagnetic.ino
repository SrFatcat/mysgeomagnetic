/*
 Name:		mysGeoMagnetic.ino
 Created:	26.09.2019 16:24:03
 Author:	Alex
 TODO: сделать что-то
*/

#define PROJECT "GeoMagParkingSensor"
#define VERSION "1.1"

//#define _DEBUG 1

#ifdef _DEBUG
#define MY_DEBUG
#else
#define MY_DISABLED_SERIAL
#endif // _DEBUG


#define MY_NODE_ID 20
#define MY_RADIO_NRF5_ESB

int16_t myTransportComlpeteMS;
#define MY_TRANSPORT_WAIT_READY_MS (myTransportComlpeteMS)
#define MY_SEND_RESET_REASON 252
#define MY_SEND_BATTERY 253


#include <MySensors.h>
#include <efektaGpiot.h>
#include <strongNode.h>

#define LED_STATE (5)
#define LED_READY (7)

#define PIN_STATE (12)
#define PIN_READY (20)
#define PIN_CORR  (18) // LOW =CORR
#define PIN_SLEEP (14) //LOW = wakeup HIGH = sleep
#define PIN_RESET (22) // LOW = RESET
#define PIN_NTC (3) 

#define CHILD_STATE 1
//#define CHILD_READY 2
#define CHILD_COMMAND 3
//#define CHILD_INFO 4
#define CHILD_STAT 5 //Статистика ошибок передач. Младший полубайт - всего передач с повторами, старший - всего СОВСЕМ неудачных передач

#define COMMAND_CORR 0b00001
#define COMMAND_RESET 0b00010
//#define COMMAND_ENABLE_GMS_UART_DATA 0b00100
#define COMMAND_FULL_RESET 0b01000

#define SLEEP_TIME 3598000UL // час без 2х секунд
#define SLEEP_TIME_WDT 100000UL
//#define SLEEP_TIME_NO_TRANSPORT (15*60*1000UL)

MyMessage msgState(CHILD_STATE, V_TRIPPED);
//MyMessage msgReady(CHILD_READY, V_TRIPPED);
//MyMessage msgInfo(CHILD_INFO, V_VAR1);
//MyMessage msgBatteryVoltage(CHILD_BATTERY_VOLTAGE, V_VOLTAGE);
MyMessage msgStat(CHILD_STAT, V_VAR1);


uint8_t receiveCommand = 0;
uint8_t statistic;


void strongPresentation(); 

CStrongNode strongNode(100, strongPresentation, CStrongNode::BATTERY_TYPE_2AA_TDM);
CDream interruptedSleep(1);

void doCommand(){
	if (receiveCommand){
		if (receiveCommand & COMMAND_CORR) {
			CORE_DEBUG("Correction!\n");
			digitalWrite(PIN_CORR, LOW);
			wait(300);
			digitalWrite(PIN_CORR, HIGH);
		}
		if (receiveCommand & COMMAND_RESET) {
			CORE_DEBUG("Reset GMS!\n");
			digitalWrite(PIN_RESET, LOW);
			wait(300);
			digitalWrite(PIN_RESET, HIGH);
		}
		if (receiveCommand & COMMAND_FULL_RESET) {
			CORE_DEBUG("Reset device!\n");
			hwReboot();
		}
		receiveCommand = 0;
	}
}

void before() {
	wdt_enable(SLEEP_TIME_WDT);
	hwPinMode(LED_STATE, OUTPUT_S0D1);
	hwPinMode(LED_READY, OUTPUT_S0D1);
	digitalWrite(LED_STATE, LOW);
	digitalWrite(LED_READY, LOW);

	hwPinMode(PIN_STATE, INPUT);
	hwPinMode(PIN_READY, INPUT);
	hwPinMode(PIN_NTC, INPUT);

	hwPinMode(PIN_CORR, OUTPUT_S0D1);
	//hwPinMode(PIN_SLEEP, OUTPUT);
	hwPinMode(PIN_RESET, OUTPUT_S0D1);
	
	digitalWrite(PIN_CORR, HIGH);
	digitalWrite(PIN_RESET, HIGH);
	strongNode.before();
}

void receive(const MyMessage& message) {
    if (strongNode.checkAck(message)) return;
	switch (message.sensor) {
	case CHILD_COMMAND:
		if (message.type == V_VAR1) {
			receiveCommand = message.getInt();
		}
		break;
	}
}

void strongPresentation() {
	strongNode.sendSketchInform(PROJECT, VERSION);
	strongNode.perform(CHILD_STATE, S_BINARY, "GMS1001 State");
	strongNode.perform(CHILD_COMMAND, S_CUSTOM, "GMS1001 Command");
	strongNode.perform(CHILD_STAT, S_CUSTOM, "Statistic");
}

bool aggresiveSend(){
	bool resend = false;
	bool sendOk;
	const uint32_t tStart = millis();
	while ( millis() - tStart < 10000UL){
		if (sendOk = strongNode.sendMsg(msgState.set((bool)digitalRead(PIN_STATE)), 5)) break;
		resend = true;
		sleep(2500);
	}
	if (resend){
		statistic++;
		if (!sendOk) statistic += 0x10;	
	}
	return sendOk;
}

void sendStat(){
	if (statistic == 0) return;
	if (strongNode.sendMsg(msgStat.set(statistic))) { //msgState!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		statistic = 0;
		saveState(0, statistic);
	}
}

void setup() {
	digitalWrite(LED_STATE, HIGH); 

	CORE_DEBUG("********** %s **********\n", PROJECT);
	strongNode.setup();

	statistic = loadState(0);
	if (statistic == 0xFF) {
		statistic = 0;
		saveState(0, statistic);
	}


	interruptedSleep.addPin(PIN_NTC, NRF_GPIO_PIN_NOPULL, CDream::NRF_PIN_LOW_TO_HIGH); 
	interruptedSleep.init();

  	digitalWrite(LED_READY, digitalRead(PIN_READY));
}

void loop() {
	doCommand();
	CORE_DEBUG("Sleeping....\n");
	int8_t sleepr = interruptedSleep.run(SLEEP_TIME, (SLEEP_TIME_WDT-2000), true);
	switch (sleepr) {
	case MY_WAKE_UP_BY_TIMER:
		CORE_DEBUG("MY_WAKE_UP_BY_TIMER\n");
		sendHeartbeat(true);
		strongNode.takeVoltage();
		strongNode.sendBattery();
		sendStat();
		break;
	case PIN_NTC:
		CORE_DEBUG("PIN_NTC READY=%i STATUS=%i\n", digitalRead(PIN_READY), digitalRead(PIN_STATE));
		digitalWrite(LED_READY, digitalRead(PIN_READY)); //потушим реди если горит
		digitalWrite(LED_STATE, LOW); //мигнем стайтом
		wait(100);
		digitalWrite(LED_STATE, HIGH);
		if (!aggresiveSend()){
			int errNum = 8;
			for (int i = 0; i < errNum; i++){
				digitalWrite(LED_STATE, LOW);
				wait(20);
				digitalWrite(LED_STATE, HIGH);
				if (i < errNum-1 ) wait(100);
			}
		}
		break;
	}
}
