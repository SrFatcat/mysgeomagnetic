# 1 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino"
/*

 Name:		mysGeoMagnetic.ino

 Created:	26.09.2019 16:24:03

 Author:	Alex

 TODO: сделать что-то

*/
# 11 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino"
//#define _DEBUG 1
# 23 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino"
int16_t myTransportComlpeteMS;





# 30 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino" 2
# 31 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino" 2
# 32 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino" 2
# 44 "c:\\Users\\Alex\\Documents\\ArduinoProject\\!NRF5\\mysGeoMagnetic\\mysGeoMagnetic\\mysGeoMagnetic.ino"
//#define CHILD_READY 2

//#define CHILD_INFO 4




//#define COMMAND_ENABLE_GMS_UART_DATA 0b00100




//#define SLEEP_TIME_NO_TRANSPORT (15*60*1000UL)

MyMessage msgState(1, V_TRIPPED);
//MyMessage msgReady(CHILD_READY, V_TRIPPED);
//MyMessage msgInfo(CHILD_INFO, V_VAR1);
//MyMessage msgBatteryVoltage(CHILD_BATTERY_VOLTAGE, V_VOLTAGE);
MyMessage msgStat(5 /*Статистика ошибок передач. Младший полубайт - всего передач с повторами, старший - всего СОВСЕМ неудачных передач*/, V_VAR1);


uint8_t receiveCommand = 0;
uint8_t statistic;


void strongPresentation();

CStrongNode strongNode(100, strongPresentation, CStrongNode::BATTERY_TYPE_2AA_TDM);
CDream interruptedSleep(1);

void doCommand(){
 if (receiveCommand){
  if (receiveCommand & 0b00001) {
   /*!< debug NULL*/;
   digitalWrite((18) /* LOW =CORR*/, (0x0));
   wait(300);
   digitalWrite((18) /* LOW =CORR*/, (0x1));
  }
  if (receiveCommand & 0b00010) {
   /*!< debug NULL*/;
   digitalWrite((22) /* LOW = RESET*/, (0x0));
   wait(300);
   digitalWrite((22) /* LOW = RESET*/, (0x1));
  }
  if (receiveCommand & 0b01000) {
   /*!< debug NULL*/;
   hwReboot();
  }
  receiveCommand = 0;
 }
}

void before() {
 ((NRF_WDT_Type *) 0x40010000UL)->CONFIG = ((NRF_WDT_Type *) 0x40010000UL)->CONFIG = ((0UL) /*!< Pause watchdog while the CPU is halted by the debugger */ << (3UL) /*!< Position of HALT field. */) | ( (1UL) /*!< Keep the watchdog running while the CPU is sleeping */ << (0UL) /*!< Position of SLEEP field. */); ((NRF_WDT_Type *) 0x40010000UL)->CRV = (32768*100000UL)/1000; ((NRF_WDT_Type *) 0x40010000UL)->RREN |= (0x1UL << (0UL) /*!< Position of RR0 field. */) /*!< Bit mask of RR0 field. */; ((NRF_WDT_Type *) 0x40010000UL)->TASKS_START = 1;
 nrf5_pinMode((5), (0x16));
 nrf5_pinMode((7), (0x16));
 digitalWrite((5), (0x0));
 digitalWrite((7), (0x0));

 nrf5_pinMode((12), (0x0));
 nrf5_pinMode((20), (0x0));
 nrf5_pinMode((3), (0x0));

 nrf5_pinMode((18) /* LOW =CORR*/, (0x16));
 //hwPinMode(PIN_SLEEP, OUTPUT);
 nrf5_pinMode((22) /* LOW = RESET*/, (0x16));

 digitalWrite((18) /* LOW =CORR*/, (0x1));
 digitalWrite((22) /* LOW = RESET*/, (0x1));
 strongNode.before();
}

void receive(const MyMessage& message) {
    if (strongNode.checkAck(message)) return;
 switch (message.sensor) {
 case 3:
  if (message.type == V_VAR1) {
   receiveCommand = message.getInt();
  }
  break;
 }
}

void strongPresentation() {
 strongNode.sendSketchInform("GeoMagParkingSensor", "1.1");
 strongNode.perform(1, S_BINARY, "GMS1001 State");
 strongNode.perform(3, S_CUSTOM, "GMS1001 Command");
 strongNode.perform(5 /*Статистика ошибок передач. Младший полубайт - всего передач с повторами, старший - всего СОВСЕМ неудачных передач*/, S_CUSTOM, "Statistic");
}

bool aggresiveSend(){
 bool resend = false;
 bool sendOk;
 const uint32_t tStart = millis();
 while ( millis() - tStart < 10000UL){
  if (sendOk = strongNode.sendMsg(msgState.set((bool)digitalRead((12))), 5)) break;
  resend = true;
  sleep(2500);
 }
 if (resend){
  statistic++;
  if (!sendOk) statistic += 0x10;
 }
 return sendOk;
}

void sendStat(){
 if (statistic == 0) return;
 if (strongNode.sendMsg(msgState.set(statistic))) {
  statistic = 0;
  saveState(0, statistic);
 }
}

void setup() {
 digitalWrite((5), (0x1));

 /*!< debug NULL*/;
 strongNode.setup();

 statistic = loadState(0);
 if (statistic == 0xFF) {
  statistic = 0;
  saveState(0, statistic);
 }


 interruptedSleep.addPin((3), NRF_GPIO_PIN_NOPULL, CDream::NRF_PIN_LOW_TO_HIGH);
 interruptedSleep.init();

   digitalWrite((7), digitalRead((20)));
}

void loop() {
 doCommand();
 /*!< debug NULL*/;
 int8_t sleepr = interruptedSleep.run(3598000UL /* час без 2х секунд*/, (100000UL -2000), true);
 switch (sleepr) {
 case ((int8_t)-1) /*!< Sleeping wake up by timer*/:
  /*!< debug NULL*/;
  sendHeartbeat(true);
  strongNode.takeVoltage();
  strongNode.sendBattery();
  sendStat();
  break;
 case (3):
  /*!< debug NULL*/;
  digitalWrite((7), digitalRead((20))); //потушим реди если горит
  digitalWrite((5), (0x0)); //мигнем стайтом
  wait(100);
  digitalWrite((5), (0x1));
  if (!aggresiveSend()){
   int errNum = 8;
   for (int i = 0; i < errNum; i++){
    digitalWrite((5), (0x0));
    wait(20);
    digitalWrite((5), (0x1));
    if (i < errNum-1 ) wait(100);
   }
  }
  break;
 }
}
