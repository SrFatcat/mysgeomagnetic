# MySensors датчик парковки на основе геомагнитного сенсора GMS1001
Назначение: открывать ворота при отъезде машины с места парковки, посредством контроллера MajorDoMo

Документация по сенсору (datasheet) [здесь](https://bitbucket.org/SrFatcat/mysgeomagnetic/src/master/docs/L47%20GMS1001%20%20Manual.docx), методика тестирования от производителя [здесь](https://bitbucket.org/SrFatcat/mysgeomagnetic/src/master/docs/L47%20GMS1001%20Test%20instruction.docx).

### Процесс разработки
![schematic](image/underconstract.jpg)
Процесс был не особо увлекателен, т.к. по большому гамбургскому счету, упрощенно сенсор сводится к сухому контакту. Самое большое достижение - это общение с сенсором по UART, в результате чего возникли процедурки расшифровки и
дешифровки байтовой абракадабры сенсора.

В МК:

```cpp
			if (getPayloadByte(payload, 0) == 0xAA) {
				length = getPayloadByte(payload, 2);
				signalCode = getPayloadByte(payload, 4);
				//CORE_DEBUG("Length: %i, Signal code: %i\n %s", length, signalCode, (length > 0) ? "Payload: " : "");
				Serial.write(0xAA);
				Serial.write(length);
				Serial.write(signalCode);
				crc = length ^ signalCode;
				for (int i = 0; i < length; i++) {
					uint8_t dataByte = getPayloadByte(payload, 6 + i*2);
					crc ^= dataByte;
					CORE_DEBUG("%i (0x%02X) ", dataByte, dataByte);
					Serial.write(dataByte);
                }
				Serial.write(crc);
```
В MajorDoMo:

```php
			$str = $this->getProperty("status");
			$frameHeader = hexdec(substr($str,0,2));
			$length = hexdec(substr($str,2,2));
		  $signalCode = hexdec(substr($str,4,2));
		  echo "Frame header: ".sprintf("%02X", $frameHeader)." Length: ".$length."(".sprintf("%02X",$length).") Signal code: ".sprintf("%02X",$signalCode)."<br>";
		  $id = array();
		  if ($length == 8){
		   echo "ID: ";
		   for ($i = 6; $i <=12; $i+=2){
			$id[] = hexdec(substr($str,$i,2));
		   }
		   foreach ($id as $ids){
			echo sprintf("%02X", $ids)." ";
		   }
		   echo '<br>';
		   $status = hexdec(substr($str,14,2));
		   $battery = hexdec(substr($str,16,2)) / 10.;
		   $magneticField = hexdec(substr($str,20,2)) << 8 | hexdec(substr($str,18,2));
		   echo "Status: ".$status.", battery ".$battery."V, magnetic field: ".$magneticField.'<br>';
		  }
		  else {
		   if ($length > 0) echo "Data byte[s]: ";
		   for ($i = 0; $i < $length; $i++){
			$dataByte = hexdec(substr($str, 6 + $i *2, 2));
			echo sprintf("%02X", $dataByte)." ";
		   }
		   if ($length > 0) echo "<br>";
		  }
		  $crc = $length;
		  for($i = 4; $i< 4 + ($length+1)*2; $i+=2){
		   $crc ^= hexdec(substr($str, $i, 2));
		   //echo $i. " = " .hexdec(substr($str, $i, 2)).'<br>';
		  }
		  $reciveCRC =  hexdec(substr($str, 4 + ($length+1)*2, 2));
		  echo "CRC: ".sprintf("%02X", $reciveCRC).' is '. ($reciveCRC == $crc? "OK" : "ERR (" . sprintf("%02X", $crc). ")"); 
```

### Реализованные возможности
 - Отсылка состояние сенсора парковки (машина есть / нет) при изменении с "умным" подтверждением доставки и попытками.
 - Удаленный перезапуск сенсора, удаленная калибровка, удаленный перезапуск всего датчика, ~~удаленное включение UART сенсора~~.
 - ~~Получение внутренних данных сенсора через его UART и отправка их на контроллер~~
 - ~~Тонкое управление сенсором командами с контроллера через UART сенсора~~
 - Hearbeat раз в час
 - Сторожевой  таймер
 - Отправка причины перезагрузки при старте
 - Отправка при пороговом изменении напряжения источника питания и состояния батареи
 
 В текущей редакции от зачеркнутых возможностей принято решение отказаться, т.к. практического смысла в них не обнаружено ))))
 
### Аппаратная реализация 
Предельно простая. Но основе модуля с МК NRF52832 от Ebyte

![schematic](image/schematic.png)

Печатная плата разработана в easyEDA, для изготовления фотополимерным способом. Предполагалась установка в взрывозащитный пластиковый корпус от двухкнопочного поста с последующей замуровкой в тратуарную плитку.

![pcb](image/pcb.png)

Однако, когда датчик был практически готов, **@EfektaSB (Andrew)** в телеграмм-канале [@DIYDEV](https://t.me/diy_devices) подарил идею с корпусом получше. Под корпус "тротоуарного светильника" пришлось плату датчика изрядно обкорнать и она обросла ~~соплями~~ перемычками. Размыкатель питания переехал на другую сторону платы и стал выключателем, а холдер с батарейкой 14250 был заменен на две батарейки АА в выносном держателе и разместился под платой. Финально это выглядит так:
![pcb](image/view1.jpg)

Окончательно замуровано (но с возможностью обслуживания) так
![pcb](image/view2.jpg)

В мажоре это выглядит так:
![pcb](image/major1.png)
![pcb](image/major2.png)

Ну и осталось дописать в `mysGMSState_1.statusUpdated`
```cpp
        $status = $this->getProperty('Status');
        if ($status != $this->getProperty('prevStatus')){
            $this->setProperty('prevStatus', $status);
            if ($status == 0 && gg("ObjGateMain.state") !=1 ) {
                runScript("КнопкаВоротBlynk");
            }
        }
```
На этом всё.